import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrentweatherComponent } from './currentweather/currentweather.component';
import { ForecastComponent } from './forecast/forecast.component';


const routes: Routes = [
  { path: 'currentweather', component: CurrentweatherComponent },
  { path: 'forecast', component: ForecastComponent },
  { path: '', component: CurrentweatherComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
