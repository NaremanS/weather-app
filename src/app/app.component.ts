import { Component } from '@angular/core';
import * as moment from 'moment';
// import moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weather-app';
  status= 'c';

  clickEvent(v){
    this.status = v;       
}
}
