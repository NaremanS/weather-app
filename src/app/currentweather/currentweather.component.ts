import { Component, OnInit } from '@angular/core';
import { CurrentweatherserviceService } from '../../assets/currentweatherservice.service';
import * as moment from 'moment';

@Component({
  selector: 'app-currentweather',
  templateUrl: './currentweather.component.html',
  styleUrls: ['./currentweather.component.css']
})
export class CurrentweatherComponent implements OnInit {

  constructor(private currentweatherservice: CurrentweatherserviceService) { }

  iconURL = 'https://openweathermap.org/img/w/';
  cityname;
  weatherData;
  show= false;
  loading= false;
  now;

  ngOnInit() {
  }
  getcityweather(){
    console.log(this.cityname);
    this.loading = true;
    return this.currentweatherservice.getweatherdata(this.cityname).subscribe(
      (data)=> {
        console.log(data); 
        this.weatherData = data; 
        this.show = true; 
        this.loading=false,
        this.now = moment();
      },
      (error)=> {console.log(error); this.show = false; this.loading=false}
    );
  }
}
