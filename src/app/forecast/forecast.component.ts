import { Component, OnInit } from '@angular/core';
import { ForecastService } from '../../assets/forecast.service';
import * as moment from 'moment';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  constructor(private forecastservice: ForecastService) { }

  iconURL = 'https://openweathermap.org/img/w/';
  cityname;
  weatherData;
  show= false;
  loading= false;
  now;
  ngOnInit() {
  }
  getcityweather(){
    console.log(this.cityname);
    this.loading= true;
    this.now = moment();
    return this.forecastservice.getforecastdata(this.cityname).subscribe(
      (data)=> {console.log(data); this.weatherData = data; this.show = true; this.loading= false},
      (error)=> {console.log(error); this.show = false; this.loading= false}
    );
  }

}
