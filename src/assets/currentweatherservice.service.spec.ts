/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CurrentweatherserviceService } from './currentweatherservice.service';

describe('Service: Currentweatherservice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CurrentweatherserviceService]
    });
  });

  it('should ...', inject([CurrentweatherserviceService], (service: CurrentweatherserviceService) => {
    expect(service).toBeTruthy();
  }));
});
