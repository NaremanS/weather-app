import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {


  constructor(private http: HttpClient) { }

  apiUrl= 'https://api.openweathermap.org/data/2.5/forecast?';
  
  
  getforecastdata(cityname) {
    const options = { params: new HttpParams()
     .set('q', cityname) 
     .set('APPID', 'eb03b1f5e5afb5f4a4edb40c1ef2f534') 
     .set('units', 'metric')
    } ;
    return this.http.get(this.apiUrl, options);
  }
  

}
